const btnToggleMenu = document.querySelectorAll(".btn-toggle-menu");
const slidingMenu = document.querySelector(".sliding-menu");

// /*
//	BEST PRACTICE:
// 	Wenn möglich mit CSS keyframes bzw. transition animieren. Dh. JS schaltet
// 	meistens nur Klassen ein/aus (toggle)
//  Achtung, transition in css aktivieren!
// */
// btnToggleMenu.forEach((el) =>
// 	el.addEventListener("click", (e) =>
// 		slidingMenu.classList.toggle("show")
// 	)
// );

/*
	Animieren mit requestAnimationFrame
*/
const stepSize = 2;
const start = -100;
const end = 0;
let currentPos = start;

function animateSlidingMenu() {
	if(currentPos === start ){
		animateIn();
	}
	else if (currentPos === end) {
		animateOut();
	}
}

function animateIn() {
	if (currentPos < end) {
		currentPos += stepSize;
		slidingMenu.style.transform = `translateX(${currentPos}%)`;
		requestAnimationFrame(animateIn);
	} else {
		// Genau auf 0 landen
		currentPos = end;
	}
}

function animateOut() {
	if (currentPos > start) {
		currentPos -= stepSize;
		slidingMenu.style.transform = `translateX(${currentPos}%)`;
		requestAnimationFrame(animateOut);
	} else {
		// Genau auf 0 landen
		currentPos = start;
	}
}

btnToggleMenu.forEach((el) => el.addEventListener("click", animateSlidingMenu));
